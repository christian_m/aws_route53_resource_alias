resource "aws_route53_record" "default" {
  allow_overwrite = var.allow_overwrite
  zone_id         = var.zone_id
  name            = var.record.name
  type            = var.record.type

  dynamic "alias" {
    for_each = var.aliases
    content {
      evaluate_target_health = alias.value.evaluate_target_health
      name                   = alias.value.name
      zone_id                = alias.value.zone_id
    }
  }
}