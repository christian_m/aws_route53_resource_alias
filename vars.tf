variable "environment" {
  description = "environment where this resource is used"
  type        = string
}

variable "zone_id" {
  description = "id of the dns zone"
  type        = string
}

variable "allow_overwrite" {
  description = "allow overwrite this record if exists"
  type        = bool
}

variable "record" {
  description = "domain record to register"
  type        = object({
    name = string,
    type = string,
  })
}

variable "aliases" {
  description = "domain alias for this record"
  type        = set(object({
    evaluate_target_health = bool
    name                   = string
    zone_id                = string
  }))
}